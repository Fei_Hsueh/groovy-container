package com.ivyxjc.gc.core;

import com.ivyxjc.gc.utils.ResourceUtils;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class GroovyExecutorService {


    public Integer execute(String filePath) throws IOException, IllegalAccessException, InstantiationException {
        String code = ResourceUtils.getGroovyCode(filePath);
        GroovyClassLoader cl = new GroovyClassLoader();
        Class clz = cl.parseClass(code);
        GroovyObject ins = (GroovyObject) clz.newInstance();
        return (Integer) ins.invokeMethod("process", null);
    }
}

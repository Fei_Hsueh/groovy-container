package com.ivyxjc.gc.core;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SharedService implements ApplicationContextAware {


    public static ApplicationContext getCtx() {
        return AppCtxHoder.getContext();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        AppCtxHoder.setContext(applicationContext);
    }


    private static class AppCtxHoder {
        private static ApplicationContext context;

        private static ApplicationContext getContext() {
            return context;
        }

        private static void setContext(ApplicationContext ctx) {
            context = ctx;
        }
    }

}

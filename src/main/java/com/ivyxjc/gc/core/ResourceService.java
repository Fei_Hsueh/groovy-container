package com.ivyxjc.gc.core;

import com.google.gson.Gson;
import com.ivyxjc.gc.model.GithubContent;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
public class ResourceService {
    private static final Logger log = LoggerFactory.getLogger(ResourceService.class);

    @Autowired
    private Environment env;

    private Gson gson = new Gson();

    private String reporUrl;

    private OkHttpClient okHttpClient = new OkHttpClient.Builder().build();

    @NonNull
    private String repoUrl() {
        if (StringUtils.isNotBlank(reporUrl)) {
            return this.reporUrl;
        }
        String server = env.getRequiredProperty("repo.server");
        String owner = env.getRequiredProperty("repo.owner");
        String name = env.getRequiredProperty("repo.name");
        return "https://api.".concat(server).concat("/repos/").concat(owner).concat("/").concat(name).concat("/contents/");
    }

    @NonNull
    public GithubContent getContent(String filePath) throws IOException {
        String token = env.getRequiredProperty("repo.token");
        String url = repoUrl().concat(filePath);
        System.out.println(url);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .header("Authorization", "token " + token)
                .build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            if (response.code() != 200 || response.body() == null) {
                throw new RuntimeException(String.format("Fail to get content from %s", url));
            }
            String respStr = Objects.requireNonNull(response.body()).string();
            return gson.fromJson(respStr, GithubContent.class);
        }
    }
}

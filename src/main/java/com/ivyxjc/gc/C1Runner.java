//package com.ivyxjc.gc;
//
//import com.ivyxjc.gc.core.ResourceService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//
//@Component
//public class C1Runner implements CommandLineRunner {
//    @Autowired
//    private ResourceService resourceService;
//
//    @Override
//    public void run(String... args) throws Exception {
//        System.out.println("++++++++++++++++");
//        resourceService.getContent("IntelliLang.xml");
//    }
//}

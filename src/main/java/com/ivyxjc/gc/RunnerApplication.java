package com.ivyxjc.gc;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import java.io.IOException;

@SpringBootApplication
@EnableJms
public class RunnerApplication {

    public static void main(String[] args) throws IOException, IllegalAccessException, InstantiationException {
//        String data="LmtvdGxpbi5qdm0nIHZlcnNpb24gJzEuMy40MScKfQoKZ3JvdXAgJ2NvbS5p";
//        byte[] res = Base64.getDecoder().decode(data.getBytes());
//        System.out.println(new String(res));
//        System.out.println("+++++++++++++++");
        SpringApplication.run(RunnerApplication.class);
//        GroovyClassLoader cl = new GroovyClassLoader();
//        Class clz = cl.parseClass(IOUtils.toString(RunnerApplication.class.getClassLoader().getResourceAsStream("aaa.groovy")));
//        GroovyObject ins = (GroovyObject) clz.newInstance();
//        Integer res = (Integer) ins.invokeMethod("process", null);


    }
}


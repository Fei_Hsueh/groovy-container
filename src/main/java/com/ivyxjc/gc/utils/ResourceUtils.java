package com.ivyxjc.gc.utils;

import com.ivyxjc.gc.core.ResourceService;
import com.ivyxjc.gc.core.SharedService;
import com.ivyxjc.gc.model.GithubContent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.util.Assert;

import java.io.IOException;

public class ResourceUtils {

    @NonNull
    public static String getResource(@NonNull String filePath) throws IOException {
        if (StringUtils.isBlank(filePath)) {
            return StringUtils.EMPTY;
        }
        ResourceService resourceService = SharedService.getCtx().getBean(ResourceService.class);
        Assert.notNull(resourceService, "ResourceService is null");
        GithubContent githubContent = resourceService.getContent("src/biz/resources/" + filePath);
        return GithubUtils.parseEncodedContent(githubContent);
    }

    @NonNull
    public static String getGroovyCode(@NonNull String filePath) throws IOException {
        if (StringUtils.isBlank(filePath)) {
            return StringUtils.EMPTY;
        }
        ResourceService resourceService = SharedService.getCtx().getBean(ResourceService.class);
        Assert.notNull(resourceService, "ResourceService is null");
        GithubContent githubContent = resourceService.getContent("src/biz/groovy/com/ivyxjc/gc/".concat(filePath));
        return GithubUtils.parseEncodedContent(githubContent);
    }
}

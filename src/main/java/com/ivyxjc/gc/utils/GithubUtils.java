package com.ivyxjc.gc.utils;

import com.ivyxjc.gc.model.GithubContent;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Base64;

public class GithubUtils {

    private static Base64.Decoder decoder = Base64.getDecoder();

    @NonNull
    public static String parseEncodedContent(@Nullable GithubContent encodedContent) {
        if (encodedContent == null) {
            return StringUtils.EMPTY;
        }
        if (StringUtils.isBlank(encodedContent.getContent())) {
            return StringUtils.EMPTY;
        }
        if ("base64".equalsIgnoreCase(encodedContent.getEncoding())) {
            String[] splits = encodedContent.getContent().split("\n");
            StringBuilder sb = new StringBuilder();
            for (String tmp : splits) {
                sb.append(new String(decoder.decode(tmp.getBytes())));
            }
            return sb.toString();
        } else {
            throw new RuntimeException(String.format("Not support this encoding %s", encodedContent.getEncoding()));
        }
    }
}

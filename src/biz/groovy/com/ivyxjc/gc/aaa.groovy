package com.ivyxjc.gc

import com.ivyxjc.gc.core.SharedService
import com.ivyxjc.gc.utils.ResourceUtils
import org.springframework.jms.core.JmsTemplate

int process() {
    doHello()
    Auto auto = new Auto()
    auto.doSome()
    return 1
}

void doHello() {
    println("Hello World")
}

class Auto {
    void doSome() {
        println("start doSome")
        JmsTemplate jmsTemplate = SharedService.ctx.getBean(JmsTemplate.class)
        jmsTemplate.convertAndSend("IVY.TRANS1", "hello")
        jmsTemplate.convertAndSend("IVY.TRANS1", "hello")
        String templateXml = ResourceUtils.getResource("template.xml")
        println("+++++++++++")
        println(templateXml)
        println("+++++++++++")
    }
}


println(CoreCommons.now())

Auto auto = new Auto()
auto.doSome()
println("hello world")

